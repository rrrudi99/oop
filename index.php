<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih OOP - Class</title>
</head>
<body>
<?php
    require 'animal.php';
    require 'frog.php';
    require 'ape.php';

    $sheep = new Animal("Shaun");

    echo "Jenis Binatang : " .$sheep->getjenis()." <br>";
    $nama= $sheep->name;
    echo "Nama Binatang : $nama <br>";
    echo "Jumlah Kaki : $sheep->legs <br>";
    echo "Berdarah Dingin : $sheep->cold_blooded <br> <br>";


    $katak = new Frog("Buduk");
    echo "Jenis Binatang : ".$katak->getjenis()." <br>";
    $nama= $katak->getname();
    echo "Nama Binatang : $nama <br>";
    echo "Jumlah Kaki : $katak->legs <br>";
    echo "Berdarah Dingin : $katak->cold_blooded <br> <br>";

    $monyet = new Ape("Kera Sakti");
    echo "Jenis Binatang : $monyet->jenis <br>";
    $nama= $monyet->name;
    echo "Nama Binatang : $nama <br>";
    echo "Jumlah Kaki : $monyet->legs <br>";
    echo "Berdarah Dingin : $monyet->cold_blooded <br> <br>";

  
?>
</body>
</html>