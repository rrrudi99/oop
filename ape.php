<?php
    require_once 'animal.php';

    class Ape extends Animal {
        //Property
        public $jenis = "Monyet";
        public $legs = 2;
        
        //Method 
        public function yell (){
            echo "Auooooo";
        }
    }
?>