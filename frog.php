<?php
    require_once 'animal.php';

    class Frog extends Animal {
        //Property
        public $jenis = "Kodok";
        
        //Method 
        public function jump (){
            echo "Hop hop";
        }
    }
?>