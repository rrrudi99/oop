
<?php
class Animal 
{
    // Property atau atribut
    public $jenis = "Domba";
    public $name = "Shaun";
    public $legs = 4;
    public $cold_blooded = "no";

    //Methode atau Function
    public function __construct($nama)
    {
       
        $this->name=$nama;
    }

    public function getjenis()
    {
        return $this->jenis; 
    }

    public function getname()
    {
        return $this->name; 
    }
    public function getlegs()
    {
        return $this->legs; 
    }
    public function get_cold_blooded()
    {
        return $this->cold_blooded; 
    }

}

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>